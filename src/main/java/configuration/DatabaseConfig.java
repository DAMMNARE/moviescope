package configuration;

import entity.Actor;
import entity.Cinema;
import entity.Movie;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class DatabaseConfig {

    private static SessionFactory sessionFactory = null;

    private DatabaseConfig() {}

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            sessionFactory = new Configuration()
                    .configure("hibernate.cfg.xml")
                    .addAnnotatedClass(Actor.class)
                    .addAnnotatedClass(Movie.class)
                    .addAnnotatedClass(Cinema.class)
                    .buildSessionFactory();
        }
        return sessionFactory;
    }
}
