package entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Entity
@Table(name = "movie")
@Data
@AllArgsConstructor
@NoArgsConstructor

public class Movie extends BaseEntity {

    @Column(nullable = false)
    private String name;

    @Column(name = "release_date")
    private LocalDate releaseDate;

    private int duration;

    @Enumerated(value = EnumType.STRING)
    private Genres genre;

    @Column(name = "imdb_rating")
    private Double IMDbRating;
}
