package entity;

public enum Genres {
    HORROR,
    COMEDY,
    ACTION,
    ROMANCE,
    SCI_FI,
    DRAMA
}
