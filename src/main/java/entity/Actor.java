package entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Entity
@Table(name = "actor")
@Data
@NoArgsConstructor
@AllArgsConstructor

public class Actor extends BaseEntity {

    @Column(name = "first_name", nullable = false)
    private String firstname;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "year_of_birth")
    private LocalDate yearOfBirth;

    @Enumerated(value = EnumType.STRING)
    private Genres genre;

    @Column(name = "imdb_rating")
    private Double IMDbRating;
}
