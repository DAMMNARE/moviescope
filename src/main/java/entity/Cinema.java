package entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "cinema")
@Data
@NoArgsConstructor
@AllArgsConstructor

public class Cinema extends BaseEntity {

    @Column(nullable = false)
    private String name;

    private String country;
    private String city;
    private String street;

    @Column(name = "street_number")
    private Integer streetNumber;

    @Column(name = "number_of_seats")
    private Integer numberOfSeats;
}
