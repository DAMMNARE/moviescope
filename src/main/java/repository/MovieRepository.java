package repository;

import entity.Movie;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class MovieRepository {

    private final SessionFactory sessionFactory;

    public MovieRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(Movie movie) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        session.persist(movie);

        transaction.commit();
        session.close();
    }

    public List<Movie> getAll() {
        List<Movie> movies;
        Session session = sessionFactory.openSession();

        movies = session.createQuery("SELECT m FROM Movie m", Movie.class).getResultList();
        session.close();
        return movies;
    }
}
