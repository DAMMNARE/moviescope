package repository;

import entity.Cinema;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class CinemaRepository {

    private final SessionFactory sessionFactory;

    public CinemaRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void  save(Cinema cinema) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        session.persist(cinema);

        transaction.commit();
        session.close();
    }

    public List<Cinema> getAll() {
        List<Cinema> cinemas;
        Session session = sessionFactory.openSession();

        cinemas = session.createQuery("SELECT c FROM Cinema c", Cinema.class).getResultList();
        session.close();
        return cinemas;
    }
}
