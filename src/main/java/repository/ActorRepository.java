package repository;

import entity.Actor;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class ActorRepository {

    private final SessionFactory sessionFactory;

    public ActorRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(Actor actor) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        session.persist(actor);

        transaction.commit();
        session.close();
    }

    public List<Actor> getAll() {
        List<Actor> actors;
        Session session = sessionFactory.openSession();

        actors = session.createQuery("SELECT a FROM Actor a", Actor.class).getResultList();
        session.close();
        return actors;
    }
}
