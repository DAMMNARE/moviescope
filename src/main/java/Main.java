import configuration.DatabaseConfig;
import entity.*;
import repository.ActorRepository;
import repository.CinemaRepository;
import repository.MovieRepository;

import java.time.LocalDate;
import java.util.List;

public class Main {

    public static ActorRepository actorRepository =
            new ActorRepository(DatabaseConfig.getSessionFactory());

    public static CinemaRepository cinemaRepository =
            new CinemaRepository(DatabaseConfig.getSessionFactory());

    public static MovieRepository movieRepository =
            new MovieRepository(DatabaseConfig.getSessionFactory());

    public static void main(String[] args) {

        Actor actor1 = new Actor("Michael", "Keaton", LocalDate.of(1951,9,5),
                Genres.ACTION, 7.9);
        Actor actor2 = new Actor("Annie", "Murphy", LocalDate.of(1986,12,19),
                Genres.DRAMA, 9.2);
        Actor actor3 = new Actor("Josh", "Hartnett", LocalDate.of(1990,8,15),
                Genres.COMEDY, 6.6);
        Actor actor4 = new Actor("Jennifer", "Lawrence", LocalDate.of(1951,9,5),
                Genres.ACTION, 7.9);
        Actor actor5 = new Actor("Chris", "Hemsworth", LocalDate.of(1983,9,11),
                Genres.SCI_FI, 8.6);

        actorRepository.save(actor1);
        actorRepository.save(actor2);
        actorRepository.save(actor3);
        actorRepository.save(actor4);
        actorRepository.save(actor5);

        Cinema cinema1 = new Cinema("Cinema City", "Romania", "Brasov", "Nicolae Titulescu",
                15, 145);
        Cinema cinema2 = new Cinema("Cineplexx", "Romania", "Sibiu", "Soseaua Sibiului",
                5, 101);
        Cinema cinema3 = new Cinema("Cineworld", "UK", "Brighton", "Brighton Marina Village",
                2, 203);

        cinemaRepository.save(cinema1);
        cinemaRepository.save(cinema2);
        cinemaRepository.save(cinema3);

        Movie movie1 = new Movie("John Wick: Chapter 4", LocalDate.of(2023,3,22),
                160, Genres.ACTION, 7.9);
        Movie movie2 = new Movie("Interstellar", LocalDate.of(2014,10,26),
                166, Genres.SCI_FI, 8.7);
        Movie movie3 = new Movie("Joker", LocalDate.of(2019,6,27),
                180, Genres.DRAMA, 8.4);
        Movie movie4 = new Movie("The Witcher", LocalDate.of(2019,3,22),
                60, Genres.HORROR, 8.1);
        Movie movie5 = new Movie("Dune", LocalDate.of(2021,10,11),
                140, Genres.ACTION, 8.0);

        movieRepository.save(movie1);
        movieRepository.save(movie2);
        movieRepository.save(movie3);
        movieRepository.save(movie4);
        movieRepository.save(movie5);

        displayAllActors();
        displayAllMovies();
        displayAllCinemas();
    }


    public static void displayAllActors() {
        List<Actor> actors = actorRepository.getAll();
        for (Actor a : actors) {
            System.out.println("First name: " + a.getFirstname() +
                    " Last Name: " + a.getLastName() +
                    " Year of birth: " + a.getYearOfBirth() +
                    " Genre: " + a.getGenre() +
                    " IMDb Rating: " + a.getIMDbRating());
        }
    }

    public static void displayAllCinemas() {
        List<Cinema> cinemas = cinemaRepository.getAll();
        for (Cinema c : cinemas) {
            System.out.println("Name: " + c.getName() +
                    " Country: " + c.getCountry() +
                    " City: " + c.getCity() +
                    " Street: " + c.getStreet() +
                    " Number of location: " + c.getStreetNumber() +
                    " Number of seats: " + c.getNumberOfSeats());
        }

    }

    public static void displayAllMovies() {
        List<Movie> movies = movieRepository.getAll();
        for (Movie m : movies) {
            System.out.println("Name: " + m.getName() +
                    " Release date: " + m.getReleaseDate() +
                    " Duration min: " + m.getDuration() +
                    " Genre: " + m.getGenre() +
                    " IMDb Rating: " + m.getIMDbRating());
        }

    }
}
